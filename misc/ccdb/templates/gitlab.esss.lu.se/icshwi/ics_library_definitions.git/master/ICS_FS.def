###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_xxxxx                                                            ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##  
##                     FS - Flow switch with two possible digital inputs                    ##
##                                                                                          ##  
##                                                                                          ##  
############################         Version: 1.3             ################################
# Author:  Miklos Boros
# Date:    27-05-2019
# Version: v1.3
# Changes: 
# 1. Vaiable Name Unification 
############################         Version: 1.0,1.1,1.2             ########################
# Author:  Miklos Boros
# Date:    28-02-2019
# Version: v1.2
# Changes: 
# 1. Major review, 
# 2. Indent,  unit standardization, first release



############################
#  STATUS BLOCK
############################ 
define_status_block()

#Operation modes
add_digital("OpMode_FreeRun",          PV_DESC="Operation Mode FreeRun", PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OpMode_Forced",           PV_DESC="Operation Mode Forced",  PV_ONAM="True",                       PV_ZNAM="False")

#Inhibit signals (set by the PLC code, can't be changed by the OPI)
add_digital("Inhibit_Manual",          PV_DESC="Inhibit Manual Mode",    PV_ONAM="InhibitManual",              PV_ZNAM="AllowManual")
add_digital("Inhibit_Force",           PV_DESC="Inhibit Force Mode",     PV_ONAM="InhibitForce",               PV_ZNAM="AllowForce")
add_digital("Inhibit_Lock",            PV_DESC="Inhibit Locking",        PV_ONAM="InhibitLocking",             PV_ZNAM="AllowLocking")

#for OPI visualization
add_digital("EnableFreeRunBtn",        PV_DESC="Enable Free Run Button", PV_ONAM="True",                       PV_ZNAM="False")
add_digital("EnableManualBtn",         PV_DESC="Enable Manual Button",   PV_ONAM="True",                       PV_ZNAM="False")
add_digital("EnableForcedBtn",         PV_DESC="Enable Force Button",    PV_ONAM="True",                       PV_ZNAM="False")

#Transmitter value
add_digital("SwitchNoFlow",  ARCHIVE=True,            PV_DESC="End switch for no flow")
add_digital("SwitchFlowON",  ARCHIVE=True,            PV_DESC="End switch for flow on")
add_analog("FlowMeasValue","REAL",  ARCHIVE=True,    PV_DESC="Measured Flow Flow EGU",          PV_EGU="l/min")
add_analog("FlowRAW","REAL",        ARCHIVE=True,    PV_DESC="Measured Flow Flow AI" )

#Locking mechanism
add_digital("DevLocked",               PV_DESC="Device Locked",          PV_ONAM="True",                       PV_ZNAM="False")
add_analog("Faceplate_LockID",         "DINT",                           PV_DESC="Owner Lock ID")
add_analog("BlockIcon_LockID",         "DINT",                           PV_DESC="Guest Lock ID")

#Alarm signals
add_major_alarm("LatchAlarm",          "Latching of alarms",             PV_ZNAM="True")
add_major_alarm("GroupAlarm",          "GroupAlarm",                     PV_ZNAM="NominalState")
add_major_alarm("Discrepancy",         "Discrepancy",                    PV_ZNAM="NominalState")
add_major_alarm("Module_Error",        "Module_Error",                   PV_ZNAM="NominalState")


############################
#  COMMAND BLOCK
############################ 
define_command_block()

#OPI buttons
add_digital("Cmd_FreeRun",             PV_DESC="CMD: FreeRun Mode")
add_digital("Cmd_Force",               PV_DESC="CMD: Force Mode")

add_digital("Cmd_NOFLOW_ON",           PV_DESC="CMD: Force Value")
add_digital("Cmd_NOFLOW_OFF",          PV_DESC="CMD: Force Value")

add_digital("Cmd_FLOWON_ON",           PV_DESC="CMD: Force Value")
add_digital("Cmd_FLOWON_OFF",         PV_DESC="CMD: Force Value")

add_digital("Cmd_AckAlarm",            PV_DESC="CMD: Acknowledge Alarm")

add_digital("Cmd_ForceUnlock",         PV_DESC="CMD: Force Unlock Device")
add_digital("Cmd_DevLock",             PV_DESC="CMD: Lock Device")
add_digital("Cmd_DevUnlock",           PV_DESC="CMD: Unlock Device")

############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()


#Locking mechanism
add_analog("P_Faceplate_LockID",       "DINT",                           PV_DESC="Device ID after Lock")
add_analog("P_BlockIcon_LockID",       "DINT",                           PV_DESC="Device ID after Blockicon Open")