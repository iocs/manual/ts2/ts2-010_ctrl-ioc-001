###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_xxxxx                                                            ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##  
##                          LT- Level Transmitter in percent                                ##
##                                                                                          ##  
##                                                                                          ##
############################         Version: 1.0             ################################
# Author:  Emilio Asensi
# Date:    30-11-2022
# Version: v1.0
# Changes: 
# 1. Cloned from CMS, added archived and offset signals     




############################
#  STATUS BLOCK
############################ 
define_status_block()

#Operation modes
add_digital("OpMode_FreeRun",          PV_DESC="Operation Mode FreeRun", PV_ONAM="True",                       PV_ZNAM="False")
add_digital("OpMode_Forced",           PV_DESC="Operation Mode Forced",  PV_ONAM="True",                       PV_ZNAM="False")

#Inhibit signals (set by the PLC code, can't be changed by the OPI)
add_digital("Inhibit_Manual",          PV_DESC="Inhibit Manual Mode",    PV_ONAM="InhibitManual",              PV_ZNAM="AllowManual")
add_digital("Inhibit_Force",           PV_DESC="Inhibit Force Mode",     PV_ONAM="InhibitForce",               PV_ZNAM="AllowForce")
add_digital("Inhibit_Lock",            PV_DESC="Inhibit Locking",        PV_ONAM="InhibitLocking",             PV_ZNAM="AllowLocking")
add_analog("TransmitterColor",         "INT",                            PV_DESC="Transmitter color")

#for OPI visualization
add_digital("EnableFreeRunBtn",        PV_DESC="Enable Free Run Button", PV_ONAM="True",                       PV_ZNAM="False")
add_digital("EnableForcedBtn",         PV_DESC="Enable Force Button",    PV_ONAM="True",                       PV_ZNAM="False")
add_digital("EnableManualBtn",         PV_DESC="Enable Manual Button",   PV_ONAM="True",                       PV_ZNAM="False")
add_analog("ScaleLOW",                 "REAL",                           PV_DESC="Scale LOW",                  PV_EGU="%")
add_analog("ScaleHIGH",                "REAL",                           PV_DESC="Scale HIGH",                 PV_EGU="%")

#Transmitter value
add_analog("MeasValue",                "REAL",  ARCHIVE=True,            PV_DESC="Temperature Value",          PV_EGU="%")
add_analog("RAWValue",                 "REAL",  ARCHIVE=True,            PV_DESC="RAW integer scaled" )
add_analog("OffsetValue",              "REAL",  ARCHIVE=True,            PV_DESC="Offset Value",          PV_EGU="%")

#Alarm signals
add_major_alarm("LatchAlarm",          "Latching of alarms",             PV_ZNAM="True")
add_major_alarm("GroupAlarm",          "GroupAlarm",                     PV_ZNAM="NominalState")
add_major_alarm("Underrange",          "Temperature Underrange",         PV_ZNAM="NominalState")
add_major_alarm("Overrange",           "Temperature Overrange",          PV_ZNAM="NominalState")
add_major_alarm("HIHI",                "Temperature HIHI",               PV_ZNAM="NominalState")
add_minor_alarm("HI",                  "Temperature HI",                 PV_ZNAM="NominalState")
add_minor_alarm("LO",                  "Temperature LO",                 PV_ZNAM="NominalState")
add_major_alarm("LOLO",                "Temperature LOLO",               PV_ZNAM="NominalState")
add_major_alarm("IO_Error",            "IO_Error",                       PV_ZNAM="NominalState")
add_major_alarm("Module_Error",        "Module_Error",                   PV_ZNAM="NominalState")
add_major_alarm("Param_Error",         "Parameter_Error",                PV_ZNAM="NominalState")
add_major_alarm("Warm_Sensor",         "Warm Sensor",                    PV_ZNAM="NominalState")

#Feedback
add_analog("FB_ForceValue",            "REAL",                           PV_DESC="Feedback Force Temperature", PV_EGU="%")
add_analog("FB_Limit_HIHI",            "REAL", ARCHIVE=True,             PV_DESC="Feedback Limit HIHI",        PV_EGU="%")
add_analog("FB_Limit_HI",              "REAL", ARCHIVE=True,             PV_DESC="Feedback Limit HI",          PV_EGU="%")
add_analog("FB_Limit_LO",              "REAL", ARCHIVE=True,             PV_DESC="Feedback Limit LO",          PV_EGU="%")
add_analog("FB_Limit_LOLO",            "REAL", ARCHIVE=True,             PV_DESC="Feedback Limit LOLO",        PV_EGU="%")


############################
#  COMMAND BLOCK
############################ 
define_command_block()

#OPI buttons
add_digital("Cmd_FreeRun",             ARCHIVE=True,		PV_DESC="CMD: FreeRun Mode")
add_digital("Cmd_Force",               ARCHIVE=True,		PV_DESC="CMD: Force Mode")
add_digital("Cmd_ForceVal",            ARCHIVE=True,		PV_DESC="CMD: Force Value")

add_digital("Cmd_AckAlarm",            PV_DESC="CMD: Acknowledge Alarm")

add_digital("StatRLHe",				   PV_DESC="Status LHe Sensor")

############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

#Limits
add_analog("P_Limit_HIHI",             "REAL",                           PV_DESC="Limit HIHI",                 PV_EGU="%")
add_analog("P_Limit_HI",               "REAL",                           PV_DESC="Limit HI",                   PV_EGU="%")
add_analog("P_Limit_LO",               "REAL",                           PV_DESC="Limit LO",                   PV_EGU="%")
add_analog("P_Limit_LOLO",             "REAL",                           PV_DESC="Limit LOLO",                 PV_EGU="%")
add_analog("P_OffsetValue",            "REAL",                           PV_DESC="Offset Value",               PV_EGU="%")

#Forcing
add_analog("P_ForceValue",             "REAL",                           PV_DESC="Force Temperature",          PV_EGU="%")

#Direct Level contrller reading
add_analog("LC_LvlR",      "REAL",     PV_DESC="Read IOC Cont Level",              PV_EGU="%")


add_verbatim("""
record(dfanout, "[PLCF#INSTALLATION_SLOT]:#StatR")
{
field(SCAN, ".1 second")
field(DESC, "Check LT status")
field(OMSL, "closed_loop")
field(DOL, "[PLCF#INSTALLATION_SLOT]:LHeSensStatR CP")
field(DISS, "INVALID")
field(DISA, "1")
field(DISV, "0")
field(OUTA,  "[PLCF#INSTALLATION_SLOT]:StatRLHe PP MSS")
}
""")

add_verbatim("""
record(dfanout, "[PLCF#INSTALLATION_SLOT]:#LC_LvlR")
{
field(SCAN, ".1 second")
field(DESC, "Read IOC Cont Level")
field(OMSL, "closed_loop")
field(DOL, "[PLCF#INSTALLATION_SLOT]:LvlR CP")
field(DISS, "INVALID")
field(DISA, "1")
field(DISV, "0")
field(OUTA,  "[PLCF#INSTALLATION_SLOT]:LC_LvlR PP MSS")
}
""")