##############################################################################
# Type:     Cryomodules
# Project:  Test Stand 2
# DevType:  TS2:010CRM: ALARMS - Alarms 
# Author:   Emilio Asensi
# Date:	    16-05-2018
# Version:  v1.0
# Note:     Based on CrS-TICP:CRYO
##############################################################################
# Type:     Cryomodules
# Project:  Test Stand 2
# DevType:  TS2:010CRM: ALARMS - Alarms 
# Author:   Emilio Asensi
# Date:	    13-01-2021
# Version:  v2.0
# Note:     Reduced spares; Added Operation Modes Alarms and Warns
##############################################################################
# Type:     Cryomodules
# Project:  Test Stand 2
# DevType:  TS2:010CRM: ALARMS - Alarms 
# Author:   Emilio Asensi
# Date:	    25-01-2021
# Version:  v2.1
# Note:     Reduced name and description length
##############################################################################

############################
#  COMMAND BLOCK
############################ 
define_command_block()

add_digital("Cmd_AckAllAlarm",                         PV_DESC="CMD: Acknowledge All Alarms")
add_digital("Cmd_AckFSAlarm",                          PV_DESC="CMD: Acknowledge Full Stop Interlocks")
add_digital("Cmd_AckTSAlarm",                          PV_DESC="CMD: Acknowledge Temp Stop Interlocks")
add_digital("Cmd_AckSIAlarm",                          PV_DESC="CMD: Acknowledge Start Interlocks")
add_digital("Cmd_AckMjAlarm",                          PV_DESC="CMD: Acknowledge Major Alarms")
add_digital("Cmd_AckMnAlarm",                          PV_DESC="CMD: Acknowledge Minor Alarms")


############################
#  STATUS BLOCK
############################ 
define_status_block()

############################
#  Operation Modes Alarms
############################ 

add_major_alarm("OMUndef_Ala01","OM Undefined Alarm 01",           PV_ZNAM="NominalState")
add_major_alarm("OMUndef_Ala02","OM Undefined Alarm 02",           PV_ZNAM="NominalState")
add_major_alarm("OMUndef_Ala03","OM Undefined Alarm 03",           PV_ZNAM="NominalState")
add_major_alarm("OMUndef_Ala04","OM Undefined Alarm 04",           PV_ZNAM="NominalState")
add_major_alarm("OMUndef_Ala05","OM Undefined Alarm 05",           PV_ZNAM="NominalState")

add_major_alarm("OMStopped_Ala01","OM Stopped Alarm 01",           PV_ZNAM="NominalState")
add_major_alarm("OMStopped_Ala02","OM Stopped Alarm 02",           PV_ZNAM="NominalState")
add_major_alarm("OMStopped_Ala03","OM Stopped Alarm 03",           PV_ZNAM="NominalState")
add_major_alarm("OMStopped_Ala04","OM Stopped Alarm 04",           PV_ZNAM="NominalState")
add_major_alarm("OMStopped_Ala05","OM Stopped Alarm 05",           PV_ZNAM="NominalState")

add_major_alarm("OMPurging_Ala01","OM Purging Alarm 01",           PV_ZNAM="NominalState")
add_major_alarm("OMPurging_Ala02","OM Purging Alarm 02",           PV_ZNAM="NominalState")
add_major_alarm("OMPurging_Ala03","OM Purging Alarm 03",           PV_ZNAM="NominalState")
add_major_alarm("OMPurging_Ala04","OM Purging Alarm 04",           PV_ZNAM="NominalState")
add_major_alarm("OMPurging_Ala05","OM Purging Alarm 05",           PV_ZNAM="NominalState")

add_major_alarm("OMReady_Ala01","OM Ready Alarm 01",           PV_ZNAM="NominalState")
add_major_alarm("OMReady_Ala02","OM Ready Alarm 02",           PV_ZNAM="NominalState")
add_major_alarm("OMReady_Ala03","OM Ready Alarm 03",           PV_ZNAM="NominalState")
add_major_alarm("OMReady_Ala04","OM Ready Alarm 04",           PV_ZNAM="NominalState")
add_major_alarm("OMReady_Ala05","OM Ready Alarm 05",           PV_ZNAM="NominalState")

add_major_alarm("OMCoolDn4K_Ala01","OM CoolDn 4K Alarm 01",           PV_ZNAM="NominalState")
add_major_alarm("OMCoolDn4K_Ala02","OM CoolDn 4K Alarm 02",           PV_ZNAM="NominalState")
add_major_alarm("OMCoolDn4K_Ala03","OM CoolDn 4K Alarm 03",           PV_ZNAM="NominalState")
add_major_alarm("OMCoolDn4K_Ala04","OM CoolDn 4K Alarm 04",           PV_ZNAM="NominalState")
add_major_alarm("OMCoolDn4K_Ala05","OM CoolDn 4K Alarm 05",           PV_ZNAM="NominalState")

add_major_alarm("OMStandBy4K_Ala01","OM Standby 4K Alarm 01",           PV_ZNAM="NominalState")
add_major_alarm("OMStandBy4K_Ala02","OM Standby 4K Alarm 02",           PV_ZNAM="NominalState")
add_major_alarm("OMStandBy4K_Ala03","OM Standby 4K Alarm 03",           PV_ZNAM="NominalState")
add_major_alarm("OMStandBy4K_Ala04","OM Standby 4K Alarm 04",           PV_ZNAM="NominalState")
add_major_alarm("OMStandBy4K_Ala05","OM Standby 4K Alarm 05",           PV_ZNAM="NominalState")

add_major_alarm("OMCoolDn2K_Ala01","OM CoolDn 2K Alarm 01",           PV_ZNAM="NominalState")
add_major_alarm("OMCoolDn2K_Ala02","OM CoolDn 2K Alarm 02",           PV_ZNAM="NominalState")
add_major_alarm("OMCoolDn2K_Ala03","OM CoolDn 2K Alarm 03",           PV_ZNAM="NominalState")
add_major_alarm("OMCoolDn2K_Ala04","OM CoolDn 2K Alarm 04",           PV_ZNAM="NominalState")
add_major_alarm("OMCoolDn2K_Ala05","OM CoolDn 2K Alarm 05",           PV_ZNAM="NominalState")

add_major_alarm("OMStandBy2K_Ala01","OM Standby 2K Alarm 01",           PV_ZNAM="NominalState")
add_major_alarm("OMStandBy2K_Ala02","OM Standby 2K Alarm 02",           PV_ZNAM="NominalState")
add_major_alarm("OMStandBy2K_Ala03","OM Standby 2K Alarm 03",           PV_ZNAM="NominalState")
add_major_alarm("OMStandBy2K_Ala04","OM Standby 2K Alarm 04",           PV_ZNAM="NominalState")
add_major_alarm("OMStandBy2K_Ala05","OM Standby 2K Alarm 05",           PV_ZNAM="NominalState")

add_major_alarm("OMNominal2KRF_Ala01","OM Nominal 2KRF Alarm 01",           PV_ZNAM="NominalState")
add_major_alarm("OMNominal2KRF_Ala02","OM Nominal 2KRF Alarm 02",           PV_ZNAM="NominalState")
add_major_alarm("OMNominal2KRF_Ala03","OM Nominal 2KRF Alarm 03",           PV_ZNAM="NominalState")
add_major_alarm("OMNominal2KRF_Ala04","OM Nominal 2KRF Alarm 04",           PV_ZNAM="NominalState")
add_major_alarm("OMNominal2KRF_Ala05","OM Nominal 2KRF Alarm 05",           PV_ZNAM="NominalState")

add_major_alarm("OMWUp4K_Ala01","OM Warmup 4K Alarm 01",           PV_ZNAM="NominalState")
add_major_alarm("OMWUp4K_Ala02","OM Warmup 4K Alarm 02",           PV_ZNAM="NominalState")
add_major_alarm("OMWUp4K_Ala03","OM Warmup 4K Alarm 03",           PV_ZNAM="NominalState")
add_major_alarm("OMWUp4K_Ala04","OM Warmup 4K Alarm 04",           PV_ZNAM="NominalState")
add_major_alarm("OMWUp4K_Ala05","OM Warmup 4K Alarm 05",           PV_ZNAM="NominalState")

add_major_alarm("OMWUp300K_Ala01","OM Warmup 300K Alarm 01",           PV_ZNAM="NominalState")
add_major_alarm("OMWUp300K_Ala02","OM Warmup 300K Alarm 02",           PV_ZNAM="NominalState")
add_major_alarm("OMWUp300K_Ala03","OM Warmup 300K Alarm 03",           PV_ZNAM="NominalState")
add_major_alarm("OMWUp300K_Ala04","OM Warmup 300K Alarm 04",           PV_ZNAM="NominalState")
add_major_alarm("OMWUp300K_Ala05","OM Warmup 300K Alarm 05",           PV_ZNAM="NominalState")

add_major_alarm("OMWUp80K_Ala01","OM Warmup 80K Alarm 01",           PV_ZNAM="NominalState")
add_major_alarm("OMWUp80K_Ala02","OM Warmup 80K Alarm 02",           PV_ZNAM="NominalState")
add_major_alarm("OMWUp80K_Ala03","OM Warmup 80K Alarm 03",           PV_ZNAM="NominalState")
add_major_alarm("OMWUp80K_Ala04","OM Warmup 80K Alarm 04",           PV_ZNAM="NominalState")
add_major_alarm("OMWUp80K_Ala05","OM Warmup 80K Alarm 05",           PV_ZNAM="NominalState")

add_major_alarm("OMStandBy80K_Ala01","OM Standby 80K Alarm 01",           PV_ZNAM="NominalState")
add_major_alarm("OMStandBy80K_Ala02","OM Standby 80K Alarm 02",           PV_ZNAM="NominalState")
add_major_alarm("OMStandBy80K_Ala03","OM Standby 80K Alarm 03",           PV_ZNAM="NominalState")
add_major_alarm("OMStandBy80K_Ala04","OM Standby 80K Alarm 04",           PV_ZNAM="NominalState")
add_major_alarm("OMStandBy80K_Ala05","OM Standby 80K Alarm 05",           PV_ZNAM="NominalState")

add_major_alarm("OMCoolDn80K4K_Ala01","OM CoolDn 80K-4K Alarm 01",           PV_ZNAM="NominalState")
add_major_alarm("OMCoolDn80K4K_Ala02","OM CoolDn 80K-4K Alarm 02",           PV_ZNAM="NominalState")
add_major_alarm("OMCoolDn80K4K_Ala03","OM CoolDn 80K-4K Alarm 03",           PV_ZNAM="NominalState")
add_major_alarm("OMCoolDn80K4K_Ala04","OM CoolDn 80K-4K Alarm 04",           PV_ZNAM="NominalState")
add_major_alarm("OMCoolDn80K4K_Ala05","OM CoolDn 80K-4K Alarm 05",           PV_ZNAM="NominalState")

add_major_alarm("OMWUp80K300K_Ala01","OM WUp 80K-300K Alarm 01",           PV_ZNAM="NominalState")
add_major_alarm("OMWUp80K300K_Ala02","OM WUp 80K-300K Alarm 02",           PV_ZNAM="NominalState")
add_major_alarm("OMWUp80K300K_Ala03","OM WUp 80K-300K Alarm 03",           PV_ZNAM="NominalState")
add_major_alarm("OMWUp80K300K_Ala04","OM WUp 80K-300K Alarm 04",           PV_ZNAM="NominalState")
add_major_alarm("OMWUp80K300K_Ala05","OM WUp 80K-300K Alarm 05",           PV_ZNAM="NominalState")

add_major_alarm("OMStarting_Ala01","OM Starting Alarm 01",           PV_ZNAM="NominalState")
add_major_alarm("OMStarting_Ala02","OM Starting Alarm 02",           PV_ZNAM="NominalState")
add_major_alarm("OMStarting_Ala03","OM Starting Alarm 03",           PV_ZNAM="NominalState")
add_major_alarm("OMStarting_Ala04","OM Starting Alarm 04",           PV_ZNAM="NominalState")
add_major_alarm("OMStarting_Ala05","OM Starting Alarm 05",           PV_ZNAM="NominalState")

add_major_alarm("OMWarmCond_Ala01","OM WarmCond Alarm 01",           PV_ZNAM="NominalState")
add_major_alarm("OMWarmCond_Ala02","OM WarmCond Alarm 02",           PV_ZNAM="NominalState")
add_major_alarm("OMWarmCond_Ala03","OM WarmCond Alarm 03",           PV_ZNAM="NominalState")
add_major_alarm("OMWarmCond_Ala04","OM WarmCond Alarm 04",           PV_ZNAM="NominalState")
add_major_alarm("OMWarmCond_Ala05","OM WarmCond Alarm 05",           PV_ZNAM="NominalState")

add_major_alarm("OMColdCond_Ala01","OM ColdCond Alarm 01",           PV_ZNAM="NominalState")
add_major_alarm("OMColdCond_Ala02","OM ColdCond Alarm 02",           PV_ZNAM="NominalState")
add_major_alarm("OMColdCond_Ala03","OM ColdCond Alarm 03",           PV_ZNAM="NominalState")
add_major_alarm("OMColdCond_Ala04","OM ColdCond Alarm 04",           PV_ZNAM="NominalState")
add_major_alarm("OMColdCond_Ala05","OM ColdCond Alarm 05",           PV_ZNAM="NominalState")


############################
#  Operation Modes Warnings
############################ 

add_minor_alarm("OMUndefined_Warn01","OM Undefined Warn 01",       PV_ZNAM="NominalState")
add_minor_alarm("OMUndefined_Warn02","OM Undefined Warn 02",       PV_ZNAM="NominalState")
add_minor_alarm("OMUndefined_Warn03","OM Undefined Warn 03",       PV_ZNAM="NominalState")
add_minor_alarm("OMUndefined_Warn04","OM Undefined Warn 04",       PV_ZNAM="NominalState")
add_minor_alarm("OMUndefined_Warn05","OM Undefined Warn 05",       PV_ZNAM="NominalState")

add_minor_alarm("OMStopped_Warn01","OM Stopped Warn 01",           PV_ZNAM="NominalState")
add_minor_alarm("OMStopped_Warn02","OM Stopped Warn 02",           PV_ZNAM="NominalState")
add_minor_alarm("OMStopped_Warn03","OM Stopped Warn 03",           PV_ZNAM="NominalState")
add_minor_alarm("OMStopped_Warn04","OM Stopped Warn 04",           PV_ZNAM="NominalState")
add_minor_alarm("OMStopped_Warn05","OM Stopped Warn 05",           PV_ZNAM="NominalState")

add_minor_alarm("OMPurging_Warn01","OM Purging Warn 01",           PV_ZNAM="NominalState")
add_minor_alarm("OMPurging_Warn02","OM Purging Warn 02",           PV_ZNAM="NominalState")
add_minor_alarm("OMPurging_Warn03","OM Purging Warn 03",           PV_ZNAM="NominalState")
add_minor_alarm("OMPurging_Warn04","OM Purging Warn 04",           PV_ZNAM="NominalState")
add_minor_alarm("OMPurging_Warn05","OM Purging Warn 05",           PV_ZNAM="NominalState")

add_minor_alarm("OMReady_Warn01","OM Ready Warn 01",           PV_ZNAM="NominalState")
add_minor_alarm("OMReady_Warn02","OM Ready Warn 02",           PV_ZNAM="NominalState")
add_minor_alarm("OMReady_Warn03","OM Ready Warn 03",           PV_ZNAM="NominalState")
add_minor_alarm("OMReady_Warn04","OM Ready Warn 04",           PV_ZNAM="NominalState")
add_minor_alarm("OMReady_Warn05","OM Ready Warn 05",           PV_ZNAM="NominalState")

add_minor_alarm("OMCoolDn4K_Warn01","OM CoolDn 4K Warn 01",           PV_ZNAM="NominalState")
add_minor_alarm("OMCoolDn4K_Warn02","OM CoolDn 4K Warn 02",           PV_ZNAM="NominalState")
add_minor_alarm("OMCoolDn4K_Warn03","OM CoolDn 4K Warn 03",           PV_ZNAM="NominalState")
add_minor_alarm("OMCoolDn4K_Warn04","OM CoolDn 4K Warn 04",           PV_ZNAM="NominalState")
add_minor_alarm("OMCoolDn4K_Warn05","OM CoolDn 4K Warn 05",           PV_ZNAM="NominalState")

add_minor_alarm("OMStandBy4K_Warn01","OM Standby 4K Warn 01",           PV_ZNAM="NominalState")
add_minor_alarm("OMStandBy4K_Warn02","OM Standby 4K Warn 02",           PV_ZNAM="NominalState")
add_minor_alarm("OMStandBy4K_Warn03","OM Standby 4K Warn 03",           PV_ZNAM="NominalState")
add_minor_alarm("OMStandBy4K_Warn04","OM Standby 4K Warn 04",           PV_ZNAM="NominalState")
add_minor_alarm("OMStandBy4K_Warn05","OM Standby 4K Warn 05",           PV_ZNAM="NominalState")

add_minor_alarm("OMCoolDn2K_Warn01","OM CoolDn 2K Warn 01",           PV_ZNAM="NominalState")
add_minor_alarm("OMCoolDn2K_Warn02","OM CoolDn 2K Warn 02",           PV_ZNAM="NominalState")
add_minor_alarm("OMCoolDn2K_Warn03","OM CoolDn 2K Warn 03",           PV_ZNAM="NominalState")
add_minor_alarm("OMCoolDn2K_Warn04","OM CoolDn 2K Warn 04",           PV_ZNAM="NominalState")
add_minor_alarm("OMCoolDn2K_Warn05","OM CoolDn 2K Warn 05",           PV_ZNAM="NominalState")

add_minor_alarm("OMStandBy2K_Warn01","OM Standby 2K Warn 01",           PV_ZNAM="NominalState")
add_minor_alarm("OMStandBy2K_Warn02","OM Standby 2K Warn 02",           PV_ZNAM="NominalState")
add_minor_alarm("OMStandBy2K_Warn03","OM Standby 2K Warn 03",           PV_ZNAM="NominalState")
add_minor_alarm("OMStandBy2K_Warn04","OM Standby 2K Warn 04",           PV_ZNAM="NominalState")
add_minor_alarm("OMStandBy2K_Warn05","OM Standby 2K Warn 05",           PV_ZNAM="NominalState")

add_minor_alarm("OMNominal2KRF_Warn01","OM Nominal 2KRF Warn 01",           PV_ZNAM="NominalState")
add_minor_alarm("OMNominal2KRF_Warn02","OM Nominal 2KRF Warn 02",           PV_ZNAM="NominalState")
add_minor_alarm("OMNominal2KRF_Warn03","OM Nominal 2KRF Warn 03",           PV_ZNAM="NominalState")
add_minor_alarm("OMNominal2KRF_Warn04","OM Nominal 2KRF Warn 04",           PV_ZNAM="NominalState")
add_minor_alarm("OMNominal2KRF_Warn05","OM Nominal 2KRF Warn 05",           PV_ZNAM="NominalState")

add_minor_alarm("OMWUp4K_Warn01","OM Warmup 4K Warn 01",           PV_ZNAM="NominalState")
add_minor_alarm("OMWUp4K_Warn02","OM Warmup 4K Warn 02",           PV_ZNAM="NominalState")
add_minor_alarm("OMWUp4K_Warn03","OM Warmup 4K Warn 03",           PV_ZNAM="NominalState")
add_minor_alarm("OMWUp4K_Warn04","OM Warmup 4K Warn 04",           PV_ZNAM="NominalState")
add_minor_alarm("OMWUp4K_Warn05","OM Warmup 4K Warn 05",           PV_ZNAM="NominalState")

add_minor_alarm("OMWUp300K_Warn01","OM Warmup 300K Warn 01",           PV_ZNAM="NominalState")
add_minor_alarm("OMWUp300K_Warn02","OM Warmup 300K Warn 02",           PV_ZNAM="NominalState")
add_minor_alarm("OMWUp300K_Warn03","OM Warmup 300K Warn 03",           PV_ZNAM="NominalState")
add_minor_alarm("OMWUp300K_Warn04","OM Warmup 300K Warn 04",           PV_ZNAM="NominalState")
add_minor_alarm("OMWUp300K_Warn05","OM Warmup 300K Warn 05",           PV_ZNAM="NominalState")

add_minor_alarm("OMWUp80K_Warn01","OM Warmup 80K Warn 01",           PV_ZNAM="NominalState")
add_minor_alarm("OMWUp80K_Warn02","OM Warmup 80K Warn 02",           PV_ZNAM="NominalState")
add_minor_alarm("OMWUp80K_Warn03","OM Warmup 80K Warn 03",           PV_ZNAM="NominalState")
add_minor_alarm("OMWUp80K_Warn04","OM Warmup 80K Warn 04",           PV_ZNAM="NominalState")
add_minor_alarm("OMWUp80K_Warn05","OM Warmup 80K Warn 05",           PV_ZNAM="NominalState")

add_minor_alarm("OMStandBy80K_Warn01","OM Standby 80K Warn 01",           PV_ZNAM="NominalState")
add_minor_alarm("OMStandBy80K_Warn02","OM Standby 80K Warn 02",           PV_ZNAM="NominalState")
add_minor_alarm("OMStandBy80K_Warn03","OM Standby 80K Warn 03",           PV_ZNAM="NominalState")
add_minor_alarm("OMStandBy80K_Warn04","OM Standby 80K Warn 04",           PV_ZNAM="NominalState")
add_minor_alarm("OMStandBy80K_Warn05","OM Standby 80K Warn 05",           PV_ZNAM="NominalState")

add_minor_alarm("OMCoolDn80K4K_Warn01","OM CoolDn 80K-4K Warn 01",           PV_ZNAM="NominalState")
add_minor_alarm("OMCoolDn80K4K_Warn02","OM CoolDn 80K-4K Warn 02",           PV_ZNAM="NominalState")
add_minor_alarm("OMCoolDn80K4K_Warn03","OM CoolDn 80K-4K Warn 03",           PV_ZNAM="NominalState")
add_minor_alarm("OMCoolDn80K4K_Warn04","OM CoolDn 80K-4K Warn 04",           PV_ZNAM="NominalState")
add_minor_alarm("OMCoolDn80K4K_Warn05","OM CoolDn 80K-4K Warn 05",           PV_ZNAM="NominalState")

add_minor_alarm("OMWUp80K300K_Warn01","OM WUp 80K-300K Warn 01",           PV_ZNAM="NominalState")
add_minor_alarm("OMWUp80K300K_Warn02","OM WUp 80K-300K Warn 02",           PV_ZNAM="NominalState")
add_minor_alarm("OMWUp80K300K_Warn03","OM WUp 80K-300K Warn 03",           PV_ZNAM="NominalState")
add_minor_alarm("OMWUp80K300K_Warn04","OM WUp 80K-300K Warn 04",           PV_ZNAM="NominalState")
add_minor_alarm("OMWUp80K300K_Warn05","OM WUp 80K-300K Warn 05",           PV_ZNAM="NominalState")

add_minor_alarm("OMStarting_Warn01","OM Starting Warn 01",           PV_ZNAM="NominalState")
add_minor_alarm("OMStarting_Warn02","OM Starting Warn 02",           PV_ZNAM="NominalState")
add_minor_alarm("OMStarting_Warn03","OM Starting Warn 03",           PV_ZNAM="NominalState")
add_minor_alarm("OMStarting_Warn04","OM Starting Warn 04",           PV_ZNAM="NominalState")
add_minor_alarm("OMStarting_Warn05","OM Starting Warn 05",           PV_ZNAM="NominalState")

add_minor_alarm("OMWarmCond_Warn01","OM WarmCond Warn 01",           PV_ZNAM="NominalState")
add_minor_alarm("OMWarmCond_Warn02","OM WarmCond Warn 02",           PV_ZNAM="NominalState")
add_minor_alarm("OMWarmCond_Warn03","OM WarmCond Warn 03",           PV_ZNAM="NominalState")
add_minor_alarm("OMWarmCond_Warn04","OM WarmCond Warn 04",           PV_ZNAM="NominalState")
add_minor_alarm("OMWarmCond_Warn05","OM WarmCond Warn 05",           PV_ZNAM="NominalState")

add_minor_alarm("OMColdCond_Warn01","OM ColdCond Warn 01",           PV_ZNAM="NominalState")
add_minor_alarm("OMColdCond_Warn02","OM ColdCond Warn 02",           PV_ZNAM="NominalState")
add_minor_alarm("OMColdCond_Warn03","OM ColdCond Warn 03",           PV_ZNAM="NominalState")
add_minor_alarm("OMColdCond_Warn04","OM ColdCond Warn 04",           PV_ZNAM="NominalState")
add_minor_alarm("OMColdCond_Warn05","OM ColdCond Warn 05",           PV_ZNAM="NominalState")


############################
#  Full Stop Interlocks
############################ 

define_plc_array("Full_Stop_Interlocks")
add_major_alarm("Full_Stop_001","Full Stop Interlock 1",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Full_Stop_002","Full Stop Interlock 2",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Full_Stop_003","Full Stop Interlock 3",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Full_Stop_004","Full Stop Interlock 4",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Full_Stop_005","Full Stop Interlock 5",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Full_Stop_006","Full Stop Interlock 6",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Full_Stop_007","Full Stop Interlock 7",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Full_Stop_008","Full Stop Interlock 8",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Full_Stop_009","Full Stop Interlock 9",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Full_Stop_010","Full Stop Interlock 10",PV_ONAM="NominalState",PV_ZNAM="Tripped")
end_plc_array()

############################
#  Temporary Stop Interlocks
############################ 

define_plc_array("Temp_Stop_Interlocks")
add_major_alarm("Temp_Stop_001","Temp Stop Interlock 1",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Temp_Stop_002","Temp Stop Interlock 2",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Temp_Stop_003","Temp Stop Interlock 3",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Temp_Stop_004","Temp Stop Interlock 4",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Temp_Stop_005","Temp Stop Interlock 5",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Temp_Stop_006","Temp Stop Interlock 6",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Temp_Stop_007","Temp Stop Interlock 7",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Temp_Stop_008","Temp Stop Interlock 8",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Temp_Stop_009","Temp Stop Interlock 9",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Temp_Stop_010","Temp Stop Interlock 10",PV_ONAM="NominalState",PV_ZNAM="Tripped")
end_plc_array()

############################
#  Start Interlocks
############################ 

define_plc_array("Start_Interlocks")
add_major_alarm("Start_Int_001","Start Interlock 1",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Start_Int_002","Start Interlock 2",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Start_Int_003","Start Interlock 3",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Start_Int_004","Start Interlock 4",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Start_Int_005","Start Interlock 5",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Start_Int_006","Start Interlock 6",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Start_Int_007","Start Interlock 7",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Start_Int_008","Start Interlock 8",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Start_Int_009","Start Interlock 9",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Start_Int_010","Start Interlock 10",PV_ONAM="NominalState",PV_ZNAM="Tripped")
end_plc_array()

############################
#  Major Alarms
############################ 

define_plc_array("Major_Alarms")
add_major_alarm("Major_Alarm_001","Major Alarm 1",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Major_Alarm_002","Major Alarm 2",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Major_Alarm_003","Major Alarm 3",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Major_Alarm_004","Major Alarm 4",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Major_Alarm_005","Major Alarm 5",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Major_Alarm_006","Major Alarm 6",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Major_Alarm_007","Major Alarm 7",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Major_Alarm_008","Major Alarm 8",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Major_Alarm_009","Major Alarm 9",PV_ONAM="NominalState",PV_ZNAM="Tripped")
add_major_alarm("Major_Alarm_010","Major Alarm 10",PV_ONAM="NominalState",PV_ZNAM="Tripped")
end_plc_array()

############################
#  Minor Alarms
############################ 

define_plc_array("Minor_Alarms")
add_minor_alarm("Minor_Alarm_001","Minor Alarm 1", PV_ONAM="NominalState",     PV_ZNAM="Tripped")
add_minor_alarm("Minor_Alarm_002","Minor Alarm 2", PV_ONAM="NominalState",     PV_ZNAM="Tripped")
add_minor_alarm("Minor_Alarm_003","Minor Alarm 3", PV_ONAM="NominalState",     PV_ZNAM="Tripped")
add_minor_alarm("Minor_Alarm_004","Minor Alarm 4", PV_ONAM="NominalState",     PV_ZNAM="Tripped")
add_minor_alarm("Minor_Alarm_005","Minor Alarm 5", PV_ONAM="NominalState",     PV_ZNAM="Tripped")
add_minor_alarm("Minor_Alarm_006","Minor Alarm 6", PV_ONAM="NominalState",     PV_ZNAM="Tripped")
add_minor_alarm("Minor_Alarm_007","Minor Alarm 7", PV_ONAM="NominalState",     PV_ZNAM="Tripped")
add_minor_alarm("Minor_Alarm_008","Minor Alarm 8", PV_ONAM="NominalState",     PV_ZNAM="Tripped")
add_minor_alarm("Minor_Alarm_009","Minor Alarm 9", PV_ONAM="NominalState",     PV_ZNAM="Tripped")
add_minor_alarm("Minor_Alarm_010","Minor Alarm 10", PV_ONAM="NominalState",     PV_ZNAM="Tripped")
end_plc_array()



